import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

export class DespesaFiltro {
  RED_GESTORA: number;
  ANO_VIGENTE: number;
  COD_FICHA: number;
  COD_DESPESA: string;
  page: number;
  size: number;
}

@Injectable({
  providedIn: 'root'
})
export class DespesaService {

  constructor(private http: HttpClient) { }

  despesasUrl = 'http://localhost:3002/contabilidade/despesas';


  getDespesas(filtro: DespesaFiltro): Observable<any> {
    let params = new HttpParams();

    if (filtro.page) {
      params = params.append('PAGE', filtro.page.toString());
    }

    if (filtro.size) {
      params = params.append('SIZE', filtro.size.toString());
    }

    if (filtro.RED_GESTORA) {
      params = params.append('RED_GESTORA', filtro.RED_GESTORA.toString());
    }

    if (filtro.ANO_VIGENTE) {
      params = params.append('ANO_VIGENTE', filtro.ANO_VIGENTE.toString());
    }

    if (filtro.COD_FICHA) {
      params = params.append('COD_FICHA', filtro.COD_FICHA.toString());
    }

    if (filtro.COD_DESPESA) {
      params = params.append('COD_DESPESA', filtro.COD_DESPESA);
    }

    return this.http.get<any>(`${this.despesasUrl}?`, { params });
  }
}
