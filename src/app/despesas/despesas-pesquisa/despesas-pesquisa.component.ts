import { Component, OnInit } from '@angular/core';
import { DespesaService, DespesaFiltro } from '../despesa.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-despesas-pesquisa',
  templateUrl: './despesas-pesquisa.component.html',
  styleUrls: ['./despesas-pesquisa.component.css']
})

export class DespesasPesquisaComponent implements OnInit {

  despesas: [];

  filtro = new DespesaFiltro();
  totalRecords = 0;

  constructor( private despesaService: DespesaService) { }

  ngOnInit() {
  }

  pesquisar(page = 1) {
    console.log(`chamou o pesquisar, pagina:  ${page}`);
    this.filtro.page = page;
    this.despesaService.getDespesas(this.filtro).
      subscribe(result => {
        this.despesas = result.despesas;
        this.totalRecords = result.totalRecords;

      });
  }

  mudarDePagina(event) {
    this.filtro.size = event.size;
    this.pesquisar(event.page);
  }


}
