import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DespesasPesquisaComponent } from './despesas-pesquisa/despesas-pesquisa.component';


const routes: Routes = [
  {
    path: '',
    component: DespesasPesquisaComponent // ,
    // canActivate: [AuthGuard],
    // data: { roles: ['ROLE_PESQUISAR_LANCAMENTO'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DespesasRoutingModule { }
