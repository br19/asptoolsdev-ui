import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DespesasRoutingModule } from './despesas-routing.module';
import { DespesasPesquisaComponent } from './despesas-pesquisa/despesas-pesquisa.component';
import { FormsModule } from '@angular/forms';
import { BRPaginationComponent } from '../core/componentes/brpagination/brpagination.component';


@NgModule({
  declarations: [
    DespesasPesquisaComponent,
    BRPaginationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DespesasRoutingModule
  ]
})
export class DespesasModule { }
