import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-brpagination',
  templateUrl: './brpagination.component.html',
  styleUrls: ['./brpagination.component.css']
})

export class BRPaginationComponent implements OnInit {

  page: number; // Pagina que estou atualmente
  pageSize: number;  // Quantas paginas tera no total (collectionSize dividido por pageSizeOptions)
  pageSizeOptions: number; // Quantas paginas mostrar no compoente (5, 10, 15...)
  size: number; // Quantas linhas (itens) mostrar por pagina

  @Output() mudarDePagina = new EventEmitter();

  _collectionSize: number = 0;
  pages: number[] = [];


  constructor() {
    this.page = 1;
    this.pageSizeOptions = 5;
    this.pageSize = this.collectionSize / this.pageSizeOptions;
    this.size = 10;
  }

  @Input() get collectionSize(): number {
    return this._collectionSize;
}

set collectionSize(val: number) {
    this._collectionSize = val;
    this.geraPages(this.page);
}

  geraPages(page: number) {
      this.mudarDePagina.emit({page: this.page, size: this.size});
      this.pageSize = Math.ceil(this.collectionSize / this.size);
      console.log(`${this.pageSize} this.pageSize`);
      const navegar = ((this.pageSizeOptions / 2) + 1)  < page;
      let i = 0;
      if (navegar) {
         i = page - 3;

         if (i > this.pageSize - this.pageSizeOptions) {
           i = this.pageSize - this.pageSizeOptions;
         }
      }

      let maxPage = this.pageSizeOptions + i;

      if (maxPage > this.pageSize) {
        maxPage = this.pageSize;
      }

      this.pages.length = 0;
      for (i; i < maxPage; i++) {
        const pageNumber = i + 1;
        this.pages.push(pageNumber);
      }
  }

  clickPage(page: number) {
    this.page = page;

    this.geraPages(this.page);
  }

  previousPage() {
    let page = this.page - 1;

    if (page < 1) {
      page = 1;
    }

    this.geraPages(page);
  }

  nextPage() {
    let page = this.page + 1;

    if (page > this.pageSize) {
      page = this.pageSize;
    }

    this.geraPages(page);
  }

  firstPage() {
    console.log(this.collectionSize);
    this.geraPages(1);
  }

  lastPage() {
    this.geraPages(this.pageSize);
  }

  ngOnInit() {
    console.log('oninitComponente');
    this.geraPages(this.page);
  }

}
