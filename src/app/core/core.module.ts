import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BRPaginationComponent } from './componentes/brpagination/brpagination.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { NaoAutorizadoComponent } from './nao-autorizado/nao-autorizado.component';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';



@NgModule({
  declarations: [
    BRPaginationComponent,
    NavbarComponent,
    NaoAutorizadoComponent,
    PaginaNaoEncontradaComponent

  ],
  exports: [
    BRPaginationComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
