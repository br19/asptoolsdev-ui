import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

export class ProcessoFiltro {
  RED_GESTORA: number;
  LIC_S_ANO: number;
  LIC_I_PROC: string;
  page: number;
  size: number;
}

export class Processo {
  RED_GESTORA: number;
  LIC_S_ANO: number;
  LIC_I_PROC: string;
}

@Injectable({
  providedIn: 'root'
})
export class ProcessoService {

  constructor(private http: HttpClient) { }

  processosUrl = 'http://localhost:3002/compras/';


  getProcessos(filtro: ProcessoFiltro): Observable<any> {
    let params = new HttpParams();

    if (filtro.page) {
      params = params.append('PAGE', filtro.page.toString());
    }

    if (filtro.size) {
      params = params.append('SIZE', filtro.size.toString());
    }

    if (filtro.RED_GESTORA) {
      params = params.append('RED_GESTORA', filtro.RED_GESTORA.toString());
    }

    if (filtro.LIC_S_ANO) {
      params = params.append('LIC_S_ANO', filtro.LIC_S_ANO.toString());
    }

    if (filtro.LIC_I_PROC) {
      params = params.append('LIC_I_PROC', filtro.LIC_I_PROC);
    }

    return this.http.get<any>(`${this.processosUrl}processos?`, { params });
  }

  copiarProcesso(processo: Processo): Observable<any> {
    let params = new HttpParams();

    if (processo.RED_GESTORA) {
      params = params.append('RED_GESTORA', processo.RED_GESTORA.toString());
    }

    if (processo.LIC_S_ANO) {
      params = params.append('LIC_S_ANO', processo.LIC_S_ANO.toString());
    }

    if (processo.LIC_I_PROC) {
      params = params.append('LIC_I_PROC', processo.LIC_I_PROC);
    }

    return this.http.get<any>(`${this.processosUrl}copiarProcesso?`, { params });
  }

}
