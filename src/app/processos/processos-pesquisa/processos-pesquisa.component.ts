import { Component, OnInit } from '@angular/core';
import { ProcessoFiltro, ProcessoService } from '../processo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-processos-pesquisa',
  templateUrl: './processos-pesquisa.component.html',
  styleUrls: ['./processos-pesquisa.component.css']
})
export class ProcessosPesquisaComponent implements OnInit {

  processos: [];
  processo: any;

  filtro = new ProcessoFiltro();
  totalRecords = 0;

  constructor( private processoService: ProcessoService,
               private modalService: NgbModal) { }

  ngOnInit() {
  }

  pesquisar(page = 1) {
    console.log(`chamou o pesquisar, pagina:  ${page}`);
    this.filtro.page = page;
    this.processoService.getProcessos(this.filtro).
      subscribe(result => {
        this.processos = result.processos;
        this.totalRecords = result.totalRecords;

      });
  }

  mudarDePagina(event) {
    this.filtro.size = event.size;
    this.pesquisar(event.page);
  }

  abrirModal(processo: any, modal) {
    this.processo = processo;
    this.modalService.open(modal);

  }

  fecharModal() {  }

  copiar(processo: any) {
    this.processoService.copiarProcesso(processo).
      subscribe(result => {
        console.log(result);
      });
  }


}
