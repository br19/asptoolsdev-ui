import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcessosPesquisaComponent } from './processos-pesquisa/processos-pesquisa.component';


const routes: Routes = [
  {
    path: '',
    component: ProcessosPesquisaComponent // ,
    // canActivate: [AuthGuard],
    // data: { roles: ['ROLE_PESQUISAR_LANCAMENTO'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessosRoutingModule { }
