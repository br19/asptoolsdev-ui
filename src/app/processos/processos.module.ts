import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProcessosRoutingModule } from './processos-routing.module';
import { FormsModule } from '@angular/forms';
import { ProcessosPesquisaComponent } from './processos-pesquisa/processos-pesquisa.component';
import { BRPaginationComponent } from '../core/componentes/brpagination/brpagination.component';


@NgModule({
  declarations: [
    ProcessosPesquisaComponent,
    BRPaginationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ProcessosRoutingModule
  ]
})
export class ProcessosModule { }
